/*
 * IMPLEMENTATION.cpp
 *
 *  Created on: Oct 11, 2012
 *      Author: somervil
 *
 * COPYRIGHT (C) 2005-2012
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2012 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#define RE2_EIGENHELPER_H_DEFINITION

#include <re2/eigen/eigen_util.h>
//#include <re2/matrix_conversions.h>


namespace re2
{

Eigen::VectorXd toEigen( const std::vector<double> & vector )
{
    Eigen::VectorXd output( vector.size() );

    std::copy( vector.begin(), vector.end(), output.data() );

    return output;
}

std::vector<double> toStd( const Eigen::VectorXd & vector )
{
    std::vector<double> output( vector.size() );

    std::copy( vector.data(), vector.data() + vector.size(), output.data() );

    return output;
}

Eigen::VectorXf toEigen( const std::vector<float> & vector )
{
    Eigen::VectorXf output( vector.size() );

    std::copy( vector.begin(), vector.end(), output.data() );

    return output;
}

std::vector<float> toStd( const Eigen::VectorXf & vector )
{
    std::vector<float> output( vector.size() );

    std::copy( vector.data(), vector.data() + vector.size(), output.data() );

    return output;
}


Eigen::VectorXd clamp( const Eigen::VectorXd & input, const Eigen::VectorXd & mins, const Eigen::VectorXd & maxs )
{
    Eigen::VectorXd clampedValues = input;
    clampedValues = clampedValues.array().min( maxs.array() );
    clampedValues = clampedValues.array().max( mins.array() );

    return clampedValues;
}


//Eigen::Matrix4d convertTfToEigen4x4( tf::StampedTransform & transform )
//{
//    btScalar rawBasisData[12];
//    transform.getBasis().getOpenGLSubMatrix(rawBasisData);
//
//    typedef Eigen::Matrix<double, 4, 4, Eigen::RowMajor> Matrix4x4Rm;
//    typedef Eigen::Matrix<double, 3, 4, Eigen::RowMajor> Matrix3x4Rm;
//    typedef Eigen::Matrix<double, 1, 3, Eigen::RowMajor> Vector3dRm;
//    Eigen::Matrix4d transfrom4x4;
//    transfrom4x4.topLeftCorner<4,3>()  = Eigen::Map<Matrix3x4Rm>( rawBasisData ).transpose();
//    transfrom4x4.topRightCorner<3,1>() = Eigen::Map<Vector3dRm>( transform.getOrigin().m_floats );//.cast<float>();
//    transfrom4x4.bottomRows<1>()       = Eigen::Vector4d( 0, 0, 0, 1 );
//
//    return transfrom4x4;
//}
//
//
//Eigen::Matrix3d transform3Dto2Dd( Eigen::Matrix4d & transfrom4x4 )
//{
//    Eigen::Matrix3d transform3x3;
//    transform3x3.topLeftCorner<2,2>()     = transfrom4x4.topLeftCorner<2,2>();
//    transform3x3.topRightCorner<2,1>()    = transfrom4x4.topRightCorner<2,1>();
//    transform3x3.bottomLeftCorner<1,2>()  = transfrom4x4.bottomLeftCorner<1,2>();
//    transform3x3.bottomRightCorner<1,1>() = transfrom4x4.bottomRightCorner<1,1>();
//
//    return transform3x3;
//}

}
